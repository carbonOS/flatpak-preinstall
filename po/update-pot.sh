#!/usr/bin/env bash
cd ~/.var/app/org.gnome.Builder/cache/gnome-builder/projects/flatpak-preinstall/builds/default-host-x86_64-main/
rm po/flatpak-preinstall.pot
ninja flatpak-preinstall-pot
ninja flatpak-preinstall-update-po
