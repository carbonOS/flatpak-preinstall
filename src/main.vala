namespace Preinstall {
    int main(string[] args) {
        // Init localization
        Intl.setlocale();
        Intl.bindtextdomain(GETTEXT_PACKAGE, LOCALEDIR);
        Intl.bind_textdomain_codeset(GETTEXT_PACKAGE, "UTF-8");
        Intl.textdomain(GETTEXT_PACKAGE);

        var loop = new MainLoop();
        var notif = new Notification();
        var i = 0;
        //var count=23; var title = _("Finishing Up Installation…"); var subtitle_tmpl = _("Please wait while we put some finishing touches on your new system! (%d/%d)");
        //var count=23; var title = _("Finishing User Setup…"); var subtitle_tmpl = _("Please wait while we put some finishing touches on your new user account! (%d/%d)");
        var count=5; var title = _("Finalizing Update…"); var subtitle_tmpl = _("We're finishing up after a recent update. (%d/%d)");
        notif.send(title, subtitle_tmpl.printf(0, count));
        Timeout.add(1250, () => {
            i++;
            if (i <= count)
                notif.send(title, subtitle_tmpl.printf(i, count));
            if (i > count) {
                notif.withdraw();
                (new Notification()).send("All Done!", "We've finished what we were doing", 1, 5000);
                loop.quit();
                return Source.REMOVE;
            }
            return Source.CONTINUE;
        });
        loop.run();
        return 0;
    }
}
