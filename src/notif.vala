namespace Preinstall {

    [DBus(name="org.freedesktop.Notifications")]
    interface NotifServer : DBusProxy {
        public abstract uint32 notify(string app_name,
                            uint32 replaces,
                            string icon,
                            string summary,
                            string body,
                            string[] actions,
                            HashTable<string, Variant> hints,
                            int32 expire) throws Error;
        public abstract void close_notification(uint32 id) throws Error;
    }

    class Notification : Object {
        private NotifServer proxy;
        private uint32 id = 0;

        construct {
            proxy = Bus.get_proxy_sync<NotifServer>(BusType.SESSION,
                                                    "org.freedesktop.Notifications",
                                                    "/org/freedesktop/Notifications");
        }

        public void send(string title, string subtitle, int urgency = 2, int expire = 0) {
            var props = new HashTable<string, Variant>(str_hash, str_equal);
            props["urgency"] = urgency; // Critical
            //props["transient"] = true; // Only banner

            this.id = proxy.notify("sh.carbon.FlatpakPreinstall",
                this.id, // replace prior instance of ourselves
                "sh.carbon.setup", // icon
                title, subtitle, // content
                new string[]{}, // no actions
                props, // more properties
                expire
            );
        }

        public void withdraw() {
            proxy.close_notification(this.id);
            this.id = 0;
        }
    }
}
