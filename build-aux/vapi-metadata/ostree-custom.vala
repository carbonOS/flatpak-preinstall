namespace OSTree {
    [CCode (destroy_function = "")]
	struct SysrootDeployTreeOpts {}

	class AsyncProgress : GLib.Object {
	    [CCode (cname="ostree_async_progress_get")]
	    public void @get(...);
	}
}
