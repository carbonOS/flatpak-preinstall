project(
	'flatpak-preinstall',
	'vala', 'c',
	version: '2023.1',
	meson_version: '>= 1.0.0',
	license: 'GPL-3.0-or-later'
)

gnome = import('gnome')
pkg = import('pkgconfig')
i18n = import('i18n')

# Include the VAPI directory in valac's arguments
vapi_dir = meson.current_source_dir() / 'build-aux' / 'vapi'
add_project_arguments(['--vapidir', vapi_dir], language: 'vala')

# Project default cflags
add_project_arguments([
	'-DG_LOG_DOMAIN="flatpak-preinstall"',
	'-DG_LOG_USE_STRUCTURED',
	'-DGETTEXT_PACKAGE="@0@"'.format(meson.project_name())
], language: 'c')

# Dependencies
gtk = dependency('gtk4')
adwaita = dependency('libadwaita-1')
flatpak = dependency('flatpak')


# Set-up the config data
conf_data = configuration_data({
        'localedir': get_option('prefix') / get_option('localedir'),
})

subdir('data')
subdir('po')
subdir('src')
