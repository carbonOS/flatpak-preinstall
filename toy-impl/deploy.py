#!/usr/bin/env python3
# This is the user installer side of the Flatpak pre-installer
import shutil
import os
import gi
gi.require_version("Flatpak", "1.0")
from gi.repository import GLib, Gio, Flatpak
from refs import *
from pathlib import Path

# Only run once
stamp = Path('~/.config/flatpak-preinstall-toy-impl-stamp').expanduser()
if not stamp.exists():
  stamp.touch()
else:
  print("Stamp exists.")
  exit(0)

# Don't trample existing installation
dest = Path('~/.local/share/flatpak').expanduser()
if dest.exists():
  print("Destination exists.")
  exit(1)
dest = str(dest.resolve())

# Unshare the network namespace, to disconnect ourselves
# from the internet & prevent Flatpak from pulling via inet.
# netns requries userns, so enter one and map our own user
# into the netns
import unshare # https://pypi.org/project/unshare/
from unshare import unshare, CLONE_NEWUSER, CLONE_NEWNET
pid = os.getpid()
uid, gid = os.getuid(), os.getgid()
unshare(CLONE_NEWUSER)
with open(f"/proc/{pid}/uid_map", "w") as f:
  f.write(f"{uid} {uid} 1")
with open(f"/proc/{pid}/setgroups", "w") as f:
  f.write("deny")
with open(f"/proc/{pid}/gid_map", "w") as f:
  f.write(f"{gid} {gid} 1")
unshare(CLONE_NEWNET)

# Get the Flatpak installation
flatpak_path = Gio.File.new_for_path(dest)
flatpak = Flatpak.Installation.new_for_path(flatpak_path, True)

interrupt = Gio.Cancellable()

# Register the Flathub remote
print("# Registering Flathub Repo...", flush=True)
with open("/etc/flatpak/remotes.d/flathub.flatpakrepo", "rb") as f:
  remote_data = GLib.Bytes(f.read())
flathub = Flatpak.Remote.new_from_file("flathub", remote_data)
flathub.set_collection_id("org.flathub.Stable")
flatpak.add_remote(flathub, True)

# Define Flatpak transaction progress output
done = 0
def progress(transaction, operation, progress):
  global done
  optype = {
    Flatpak.TransactionOperationType.INSTALL: "Installing",
    Flatpak.TransactionOperationType.UPDATE: "Updating",
    Flatpak.TransactionOperationType.INSTALL_BUNDLE: "Installing bundle",
    Flatpak.TransactionOperationType.UNINSTALL: "Uninstalling",
  }.get(operation.get_operation_type(), "<Unknown>")
  ref_name = Flatpak.Ref.parse(operation.get_ref()).get_name().removesuffix(".Locale")
  done += 1
  print(f"# {optype} {NAMES[ref_name]}…", flush=True)
  print(f"{(done / len(REFS)) * 100:.0f}", flush=True)

# Create the transaction
transaction = Flatpak.Transaction.new_for_installation(flatpak)
transaction.add_default_dependency_sources()
transaction.add_sideload_repo("sideload")
#transaction.set_parent_window(FOREIGN_TOPLEVEL_HANDLE)
transaction.connect("new-operation", progress)
for ref in APP_REFS:
  transaction.add_install("flathub", ref, None)

transaction.run(interrupt)
