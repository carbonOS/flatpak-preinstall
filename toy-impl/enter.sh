#!/usr/bin/env bash
export FLATPAK_USER_DIR=`pwd`/deploy
mkdir tmp
unshare -rm sh -c "mount --bind ./tmp /var/lib/flatpak && exec bash"
rmdir tmp
