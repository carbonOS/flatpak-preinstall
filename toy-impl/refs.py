NAMES = {
  "org.gnome.Epiphany": "Web",
  "org.gnome.TextEditor": "Text Editor",
  "org.gnome.Calculator": "Calculator",
  "org.gnome.Contacts": "Contacts",
  "org.gnome.Geary": "Geary",
  "org.gnome.Calendar": "Calendar",
  "org.gnome.eog": "Image Viewer",
  "org.gnome.Evince": "Document Viewer",
  "org.gnome.clocks": "Clocks",
  "org.gnome.Weather": "Weather",
  "org.gnome.Logs": "Logs",
  "org.gnome.Totem": "Videos",
  "org.gnome.baobab": "Disk Usage Analyzer",
  "org.gnome.Characters": "Characters",
  "com.mattjakeman.ExtensionManager": "Extension Manager",
  "org.gnome.font_viewer": "Font Viewer",
  "org.gnome.font-viewer": "Font Viewer",
  "org.gnome.Platform": "Runtime",
}

RUNTIME_REFS = [
  "runtime/org.gnome.Platform/x86_64/43",
  "runtime/org.gnome.Platform.Locale/x86_64/43",
#  "runtime/org.freedesktop.Platform.GL.default/x86_64/22.08",
]

APP_IDS = [
  "org.gnome.Epiphany",
  "org.gnome.TextEditor",
  "org.gnome.Calculator",
  "org.gnome.Contacts",
  "org.gnome.Geary",
  "org.gnome.Geary",
  "org.gnome.Calendar",
  "org.gnome.eog",
  "org.gnome.Evince",
  "org.gnome.clocks",
  "org.gnome.Weather",
  "org.gnome.Logs",
  "org.gnome.Totem",
  "org.gnome.baobab",
  "org.gnome.Characters",
  "com.mattjakeman.ExtensionManager",
  "org.gnome.font-viewer",
#  "org.gnome.seahorse.Application", # NEEDS GNOME42
]

APP_REFS = []
for app in APP_IDS:  
  APP_REFS.append(f"app/{app}/x86_64/stable")
  APP_REFS.append(f"runtime/{app.replace('-', '_')}.Locale/x86_64/stable")

REFS = RUNTIME_REFS + APP_REFS
